package com.webVillage.app.Framework.pages;

import com.webVillage.app.Framework.helper.TestHelper;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import ru.yandex.qatools.ashot.comparison.ImageDiff;
import ru.yandex.qatools.ashot.comparison.ImageDiffer;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

public class LandingWikiPage extends TestHelper {

    public LandingWikiPage open() {
        driver.get("https://www.wikipedia.org/");
        return this;
    }

    public LandingWikiPage typeKrokodil() {
        driver.findElement(By.cssSelector("#searchInput")).sendKeys("Krokodil");
        driver.findElement(By.cssSelector("button.pure-button.pure-button-primary-progressive")).click();
        return this;
    }

    public boolean compareScreenShot() throws IOException {
//        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        BufferedImage screenShot = null;
        screenShot = ImageIO.read(new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
        BufferedImage etalonShot = ImageIO.read(new File("/Users/user/Desktop/aqajunior-lecture_31-webFun/webFun/src/test/resources/screeWiki.png"));
        ImageDiff diff = new ImageDiffer().makeDiff(screenShot, etalonShot);
        return diff.hasDiff();
//        try {
//            FileUtils.copyFile(screenshot, new File("screeWiki.png"));
//        } catch (IOException e) {
//            e.printStackTrace();
    }
}


