package com.webVillage.app.Framework.pages;

import org.openqa.selenium.WebDriver;

public class CreatedPerson extends BasePage{

    public String getFirstName () {
       String[] FIO =  getElementById("nameLoaded").getText().split(" ");
        return FIO[1];
    }

    public String getLastName () {
        String[] FIO =  getElementById("nameLoaded").getText().split(" ");
        return FIO[2];
    }

    public String getAge () {
        String age =  getElementById("age").getText();
        return age.split(" ")[1];
    }

    public String getOccupation () {
        String occupation =  getElementById("occupation").getText();
        return occupation.split(" ")[1];
    }

    public void pushEditButton () {
        getElementById("editButton").click();
    }

    public void pushDeleteButton () {
        getElementById("deleteButton").click();
    }

    public void pushBackButton () {
        getElementById("backButton").click();
    }

    public void pushPopUpButton () {
        getElementById("popupButton").click();
    }

    public void openPersonPage (String name) {
        getElementById(name).click();
    }
}
