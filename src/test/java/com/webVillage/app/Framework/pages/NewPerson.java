package com.webVillage.app.Framework.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.List;
import java.util.Random;

public class NewPerson extends BasePage{

    Random random = new Random();

    public NewPerson(WebDriver driver){
        this.driver = driver;
        this.path = "/person/new";
    }

    public NewPerson open(){
        driver.get(basePath + path);
        return this;
    }

    public NewPerson setFirstName(String firstName){
        getElementById("firstName").sendKeys(firstName);
        return this;
    }

    public NewPerson setSecondName(String secondName){
        getElementById("secondName").sendKeys(secondName);
        return this;
    }

    public NewPerson setAge(String age){
        getElementById("age").sendKeys(age);
        return this;
    }

    public NewPerson setOccupation (String nameOfOccupation) {
        Select occupation = new Select(driver.findElement(By.id("occupation")));
        occupation.selectByVisibleText(nameOfOccupation);
        return this;
    }

    public NewPerson setGender() {
        List<WebElement> gender = driver.findElements(By.cssSelector("#gender > option"));
        gender.get(random.nextInt(gender.size() - 1)).click();
        return this;
    }

    public NewPerson clickSaveButton() {
        getElementById("doIt").click();
        return this;
    }
}
