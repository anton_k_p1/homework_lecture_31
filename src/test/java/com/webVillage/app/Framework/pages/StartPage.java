package com.webVillage.app.Framework.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.Set;

public class StartPage extends BasePage {

    public StartPage(WebDriver driver){
        this.driver = driver;
        this.path = "/";
    }

    public StartPage open(){
        driver.get(basePath + path);
        return this;
    }

    public NewPerson clickNewPerson(){
        WebElement element =  driver.findElement(By.xpath("//a[@href='/person/new']"));
        String a = element.getText();
        element.click();
        return new NewPerson(driver).open();
    }

    public PersonInfo clickToJohnDow() throws InterruptedException {
        String mainWindow = driver.getWindowHandle();
        getElementById("1").click();

        Set<String> windows = driver.getWindowHandles();
          for (String window:
                windows) {
              driver.switchTo().window(window);

              if (window.equals(mainWindow)) {

                  driver.close();
              }
          }
        return new PersonInfo(driver).open();
    }
}
