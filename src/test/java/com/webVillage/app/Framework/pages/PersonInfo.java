package com.webVillage.app.Framework.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PersonInfo extends BasePage{

    public PersonInfo(WebDriver driver) {
        this.driver = driver;
        this.path = "/person/1";
    }

    public PersonInfo open(){
        driver.get(basePath + path);
        return this;
    }

    public void clickToEdit(){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("#nameLoaded"))));
        getElementById("editButton").click();
    }
}
