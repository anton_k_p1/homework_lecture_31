package com.webVillage.app.Framework.tests;

import com.webVillage.app.Framework.helper.TestHelper;
import com.webVillage.app.Framework.pages.LandingWikiPage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;

public class WikiTest extends TestHelper {

    @Test
    public void checkWikiScreen () throws IOException {
        Assertions.assertFalse(new LandingWikiPage().open().typeKrokodil().compareScreenShot());
    }
}
