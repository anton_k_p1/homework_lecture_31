package com.webVillage.app.Framework.tests;

import com.webVillage.app.Framework.helper.TestHelper;
import com.webVillage.app.Framework.pages.StartPage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import com.webVillage.app.Framework.pages.CreatedPerson;

public class CreatedPersonTest extends TestHelper {

    @Test
    void createdPersonTest() throws InterruptedException {
        new StartPage(driver)
                .open()
                .clickNewPerson()
                .setFirstName("Ivan")
                .setSecondName("Ivanov")
                .setAge(String.valueOf(101))
                .setOccupation("Doctor")
                .setGender()
                .clickSaveButton();
        CreatedPerson createdPerson = new CreatedPerson();
        Assertions.assertEquals(createdPerson.getFirstName(), "Ivan");
        Assertions.assertEquals(createdPerson.getLastName(), "Ivanov");
        Assertions.assertEquals(createdPerson.getOccupation(), "doctor");
        Assertions.assertEquals(createdPerson.getAge(), String.valueOf(101));
    }
}
